jQuery(function($) {
	var validation_holder;

    $(".drag-drop").on('dragover',function(event){
        alert(1);

    });
    $(".drag-drop").on('dragleave',function(event){
    alert(2);

    });
    $(".drag-drop").on('drop',function(event){
       var html =
        $(".show-drag-drop").append(html);
    });

	$("form#login button[name='Login']").click(function() {
		var validation_holder = 0;
		var email 			= $("form#login input[name='email']").val();
		var email_regex 	= /^[\w%_\-.\d]+@[\w.\-]+.[A-Za-z]{2,6}$/;
		var password 		= $("form#login input[name='pass']").val();


		if(email == "") {
			$("span.val_email").html("This field is required.").addClass(' v_fail');
			validation_holder = 1;
		} else {
			if(!email_regex.test(email)){
				$("span.val_email").html("Invalid Email!").addClass(' v_fail');
				validation_holder = 1;
			} else {
				$("span.val_email").html("");
				$("span.val_email").removeClass(' v_fail');
			}
		}


		if(password == "") {
			$("span.val_pass").html("This field is required.").addClass(' v_fail');
			validation_holder = 1;
		} else {
				$("span.val_pass").html("");
				$("span.val_pass").removeClass(' v_fail');
		}

		if(validation_holder == 1) {
		    var delay = 2000;
            setTimeout(function(){ $('span.v_fail').removeClass(' v_success').removeClass(' v_fail'); }, delay);
			return false;
		}
		var ajaxReq = $.ajax('login', {
					type: 'POST',
                    data:$('form#login').serialize()+ "&Login=submit"
                });
                ajaxReq.success(function (data, status, jqXhr) {
					data = $.trim(data);
                    var obj = jQuery.parseJSON(data);
					if(obj.result==="success"){
					    $('span.val_status').html('Status :  Successfully Logged in..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
						var delay = 2000;
						var url = 'http://www.practiceblog.com';
						setTimeout(function(){ window.location = url; }, delay);
					}else{
					    if(!(obj.email_error==="")){
                            $("span.val_email").html(obj.email_error).addClass(' v_fail');
					    }
					    if(!(obj.pass_error==="")){
                            $("span.val_pass").html(obj.pass_error).addClass(' v_fail');
					    }
					    var delay = 2000;
                        setTimeout(function(){ $('span.v_fail').removeClass(' v_success').removeClass(' v_fail'); }, delay);
					}
                });
                ajaxReq.error(function (jqXhr, textStatus, errorMessage) {
					$('span.val_status').html('Something Went wrong....');
					$('span.val_status').addClass(" v_fail");
					var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_success').removeClass(' v_fail'); }, delay);

                });

		return false;

	});


	$("form#registration button[name='Register']").click(function() {
		var validation_holder = 0;
		var name 			= $("form#registration input[name='name']").val();
		var email 			= $("form#registration input[name='email']").val();
		var email_regex 	= /^[\w%_\-.\d]+@[\w.\-]+.[A-Za-z]{2,6}$/;
		var pass_regex 	=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
		var password 		= $("form#registration input[name='pass']").val();
		var repassword 		= $("form#registration input[name='repass']").val();


		if(name == "") {
			$("span.val_name").html("This field is required.").addClass(' v_fail');
			validation_holder = 1;
		} else {
			$("span.val_name").html("");
		}

		if(email == "") {
			$("span.val_email").html("This field is required.").addClass(' v_fail');
			validation_holder = 1;
		} else {
			if(!email_regex.test(email)){
				$("span.val_email").html("Invalid Email!").addClass(' v_fail');
				validation_holder = 1;
			} else {
				$("span.val_email").html("");
			}
		}


		if(password == "") {
			$("span.val_pass").html("This field is required.").addClass(' v_fail');
			validation_holder = 1;
		} else {
			if(!pass_regex.test(password)){
				$("span.val_pass").html("Week Password").addClass(' v_fail');
			}else {
				$("span.val_pass").html("");
			}
		}

		if(repassword == "") {
			$("span.val_repass").html("This field is required.").addClass(' v_fail');
			validation_holder = 1;
		} else {
			if(password != repassword) {
				$("span.val_repass").html("Password does not match!").addClass(' v_fail');
				validation_holder = 1;
			} else {
				if(!pass_regex.test(password)){
					$("span.val_repass").html("Week Password").addClass(' v_fail');
				}else {
					$("span.val_repass").html("");
				}
			}
		}

		if(validation_holder == 1) {
             var delay = 2000;
             setTimeout(function(){ $('span.v_fail').removeClass(' v_success').removeClass(' v_fail'); }, delay);
			return false;
		}
		var ajaxReq = $.ajax('join', {
					type: 'POST',
                    data:$('form#registration').serialize()+ "&Register=submit"
                });
                ajaxReq.success(function (data, status, jqXhr) {
					var obj = jQuery.parseJSON($.trim(data));
					if(obj.result=="success"){
					    $('span.val_status').html('Status :  Successfully Registered with US.');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
						var delay = 2000;
						setTimeout(function(){ $('form#registration')[0].reset(); $('span.val_status').removeClass(' v_success');}, delay);
					}else{
					    if(!(obj.email_error==="")){
                            $("span.val_email").html(obj.email_error).addClass(' v_fail');
					    }
					    if(!(obj.pass_error==="")){
                            $("span.val_pass").html(obj.pass_error).addClass(' v_fail');
					    }
					    if(!(obj.repass_error==="")){
                            $("span.val_repass").html(obj.repass_error).addClass(' v_fail');
					    }
					    if(!(obj.name_error==="")){
                            $("span.val_name").html(obj.name_error).addClass(' v_fail');
					    }
                    var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_fail'); }, delay);
					}
				});
                ajaxReq.error(function (jqXhr, textStatus, errorMessage) {
					$('span.val_status').html('Something went wrong...');
					$('span.val_status').addClass(" v_fail");
					var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_fail'); }, delay);
                });
		return false;

	});


	$("#post_request").on("click", "button", function() {
             var clickButtonId = $(this).attr('id');
             var ajaxReq = $.ajax('actions', {
					type: 'POST',
                    data:"Post_Id=" + clickButtonId + "&delete_post=submit"
                });
                ajaxReq.success(function (data, status, jqXhr) {
                    data = $.trim(data);
                    var obj = jQuery.parseJSON(data);
					if(obj.result==="success"){
					    $('span.val_status').html('Status :  Successfully Posted deleted..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                        var delay = 1000;
						var url = 'http://www.practiceblog.com/admin/post_actions';
						setTimeout(function(){ window.location = url; }, delay);
                    }else{
                        $('span.val_status').html('Status :  Failed to Delete Posted..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                    }
                    var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_success').removeClass(' v_fail'); }, delay);
                });
                ajaxReq.error(function (jqXhr, textStatus, errorMessage) {
					$('span.val_status').html('Something Went wrong....');
					$('span.val_status').addClass(" v_fail");
					var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_fail'); }, delay);
                });

		return false;
     });


      $("form#Add_Post button[name='add_post']").click(function() {
		var validation_holder = 0;
		var post_title = $("form#Add_Post input[name='post_name']").val();
		var post_regex 	= /\w+\s{1}$/;
		var post_desc = $("form#Add_Post textarea[name='post_desc']").val();
		if(post_title == "") {
			$("span.val_post_name").html("This field is required.").addClass(' v_fail');
			validation_holder = 1;
		} else {
			if(post_title.split(" ").length < 5){
				$("span.val_post_name").html("Invalid Post Title!").addClass(' v_fail');
				validation_holder = 1;
			} else {
				$("span.val_post_name").html("");
				$("span.val_post_name").removeClass(' v_fail');
			}
		}
		if(post_desc == "") {
			$("span.val_post_desc").html("This field is required.").addClass(' v_fail');
			validation_holder = 1;
		} else {
		    if(post_desc.split(" ").length < 5){
				$("span.val_post_desc").html("Invalid Post Description!").addClass(' v_fail');
				validation_holder = 1;
			} else {
				$("span.val_post_desc").html("");
				$("span.val_post_desc").removeClass(' v_fail');
			}
		}
		if(validation_holder == 1) {
			var delay = 2000;
            setTimeout(function(){ $('span.v_fail').removeClass(' v_fail'); }, delay);
            return false;
		}
            var ajaxReq = $.ajax('add_post', {
					type: 'POST',
                    data: $('form#Add_Post').serialize()+ "&add_post=submit"
                });
                ajaxReq.success(function (data, status, jqXhr) {
					data = $.trim(data);
                    var obj = jQuery.parseJSON(data);
					if(obj.result==="success"){
					    $('span.val_status').html('Status :  Successfully Posted..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
					    var delay = 2000;
						setTimeout(function(){ $('form#Add_Post')[0].reset();$('span.val_status').removeClass(' v_success'); }, delay);
					}else{
					    if(!(obj.post_name_error==="")){
                            $("span.val_post_name").html(obj.email_error).addClass(' v_fail');
					    }
					    if(!(obj.post_desc_error==="")){
                            $("span.val_post_desc").html(obj.pass_error).addClass(' v_fail');
					    }
					    var delay = 2000;
						setTimeout(function(){ $('span.v_fail').removeClass(' v_fail'); }, delay);
					}
                });
                ajaxReq.error(function (jqXhr, textStatus, errorMessage) {
					$('span.val_status').html('Something Went wrong....');
					$('span.val_status').addClass(" v_fail");
					var delay = 2000;
                    setTimeout(function(){ $('span.val_status').removeClass(' v_fail'); }, delay);
                });

		return false;

	});


	$("#users_request").on("click", "button", function() {
	         var type = $(this).attr('class');
             var id = $(this).attr('id');
             var ajaxReq = $.ajax('approve', {
					type : 'POST',
                    data : "User_Id=" + id + "&type="+ type+ "&delete_post=submit"
                });
                ajaxReq.success(function (data, status, jqXhr) {
                    data = $.trim(data);
                    var obj = jQuery.parseJSON(data);
					if(obj.result==="success"){
					    $('span.val_status').html('Status :  Successfully Approved..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                        var delay = 1000;
						var url = 'http://www.practiceblog.com/admin/all_users';
						setTimeout(function(){ window.location = url; }, delay);
                    }else if(obj.result==="success_delete"){
					    $('span.val_status').html('Status :  Successfully Rejected..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                        var delay = 1000;
						var url = 'http://www.practiceblog.com/admin/approve';
						setTimeout(function(){ window.location = url; }, delay);
                    }else{
                        $('span.val_status').html('Status :  Failed to Perform Operation..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                    }
                    var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_success').removeClass(' v_fail'); }, delay);
                });
                ajaxReq.error(function (jqXhr, textStatus, errorMessage) {
					$('span.val_status').html('Something Went wrong....');
					$('span.val_status').addClass(" v_fail");
					var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_fail'); }, delay);
                });

		    return false;
        });

    $("#all_users_request").on("click", "button", function() {
	         var type = $(this).attr('class');
             var id = $(this).attr('id');
             var ajaxReq = $.ajax('all_users', {
					type : 'POST',
                    data : "User_Id=" + id + "&type="+ type+ "&delete_post=submit"
                });
                ajaxReq.success(function (data, status, jqXhr) {
                    data = $.trim(data);
                    var obj = jQuery.parseJSON(data);
					if(obj.result==="success"){
					    $('span.val_status').html('Status :  Successfully Approved..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                        var delay = 1000;
						var url = 'http://www.practiceblog.com/admin/all_users';
						setTimeout(function(){ window.location = url; }, delay);
                    }else if(obj.result==="dis_approve"){
					    $('span.val_status').html('Status :  Successfully Rejected..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                        var delay = 1000;
						var url = 'http://www.practiceblog.com/admin/all_users';
						setTimeout(function(){ window.location = url; }, delay);
                    }else{
                        $('span.val_status').html('Status :  Failed to Perform Operation..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                    }
                    var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_success').removeClass(' v_fail'); }, delay);
                });
                ajaxReq.error(function (jqXhr, textStatus, errorMessage) {
					$('span.val_status').html('Something Went wrong....');
					$('span.val_status').addClass(" v_fail");
					var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_fail'); }, delay);
                });

		    return false;
        });

//--------------
    $("#users_requests").on("click", "button", function() {
	         var type = $(this).attr('class');
             var id = $(this).attr('id');
             var ajaxReq = $.ajax('user_actions', {
					type : 'POST',
                    data : "User_Id=" + id + "&type="+ type+ "&delete_post=submit"
                });
                ajaxReq.success(function (data, status, jqXhr) {
                    data = $.trim(data);
                    var obj = jQuery.parseJSON(data);
					if(obj.result==="success"){
					    $('span.val_status').html('Status :  Successfully Approved..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                        var delay = 1000;
						var url = 'http://www.practiceblog.com/admin/user_actions';
						setTimeout(function(){ window.location = url; }, delay);
                    }else if(obj.result==="success_delete"){
					    $('span.val_status').html('Status :  Successfully deleted..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                        var delay = 1000;
						var url = 'http://www.practiceblog.com/admin/approve';
						setTimeout(function(){ window.location = url; }, delay);
                    }else{
                        $('span.val_status').html('Status :  Failed to Perform Operation..');
					    $('span.val_status').removeClass("val_status v_fail").removeClass("val_status v_success").addClass("val_status v_success");
                    }
                    var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_success').removeClass(' v_fail'); }, delay);
                });
                ajaxReq.error(function (jqXhr, textStatus, errorMessage) {
					$('span.val_status').html('Something Went wrong....');
					$('span.val_status').addClass(" v_fail");
					var delay = 2000;
                    setTimeout(function(){ $('span.v_fail').removeClass(' v_fail'); }, delay);
                });

		    return false;
        });



//---------------

});
