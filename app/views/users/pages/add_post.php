<div class="container-login">
    <div class="wrapper-login">
        <h2>Add Post</h2>
            <span class="invalidFeedback val_status"></span>
            <form id="Add_Post" action="" method="POST">
                <input type="text" placeholder="Post Title *" name="post_name">
                <span class="invalidFeedback val_post_name"></span>

                <textarea  placeholder="Post Description *" rows="10" cols="50" name="post_desc"></textarea>
                <span class="invalidFeedback val_post_desc"></span>

                <button id="submit" type="submit" name="add_post" value="Add Post">Submit</button>
        </form>
    </div>
</div>
