<html>
<head>
	<title> <?php echo $data['title']; ?> </title>
	<link rel="stylesheet" href="<?php echo URLROOT; ?>/css/styles.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>

<body>

<div class="main_nav">
	<div class="sub_nav">
	    <?php if(!isset($_SESSION['user_name'])){ ?>
		<a href="<?php echo URLROOT; ?>/posts">Home</a>
		<a href="<?php echo URLROOT; ?>/posts/about">About</a>
		<?php } ?>
		<div class="sub_nav_right">

			<?php if(isset($_SESSION['user_name'])):
			 if($_SESSION['user_type']==0 && $_SESSION['user_live']==1){ ?>
				<span><?php echo $_SESSION['user_name'];?></span>
				<a href="<?php echo URLROOT; ?>/posts">Dashboard</a>
				<a href="<?php echo URLROOT; ?>/posts/add_post">Add Post</a>
				<a href="<?php echo URLROOT; ?>/posts/about">About</a>
				<a href="<?php echo URLROOT; ?>/users/logout">Logout</a>

			<?php } else if($_SESSION['user_type']==0 && $_SESSION['user_live']!=1){ ?>
			    <a href="<?php echo URLROOT; ?>/posts">Dashboard</a>
                <a href="<?php echo URLROOT; ?>/posts/about">About</a>
				<a href="<?php echo URLROOT; ?>/users/logout">Logout</a>
	        <?php }else if($_SESSION['user_type']==1){ ?>
                <span><?php echo $_SESSION['user_name'];?></span>
				<a href="<?php echo URLROOT; ?>/posts">Dashboard</a>
				<a href="<?php echo URLROOT; ?>/posts/add_post">Add Post</a>
				<a href="<?php echo URLROOT; ?>/admin/approve">Requests</a>
				<a href="<?php echo URLROOT; ?>/admin/all_users">All Users</a>
				<span class="div_drop_down">Actions
                    <li class="drop_down">
                        <a href="<?php echo URLROOT; ?>/admin/post_actions">Post Actions</a>
                        <a href="<?php echo URLROOT; ?>/admin/user_actions">Users Actions</a>
                    </li>
				</span>
				<a href="<?php echo URLROOT; ?>/users/logout">Logout</a>
			<?php } endif;
			if(!isset($_SESSION['user_name'])){ ?>
				<a href="<?php echo URLROOT; ?>/users/join">Join</a>
				<a href="<?php echo URLROOT; ?>/users/login">Sign in</a>
			<?php } ?>
		</div>

	</div>
</div>