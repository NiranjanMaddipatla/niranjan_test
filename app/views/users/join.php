<div class="container-login">
    <div class="wrapper-login">
        <h2>Register</h2>
            <span class="invalidFeedback val_status"></span>
            <form id="registration" method="POST" action="">
            <input type="text" placeholder="Username *" name="name">
            <span class="invalidFeedback val_name"></span>

            <input type="email" placeholder="Email *" name="email">
            <span class="invalidFeedback val_email"></span>

            <input type="password" placeholder="Password *" name="pass">
            <span class="invalidFeedback val_pass"></span>

            <input type="password" placeholder="Confirm Password *" name="repass">
            <span class="invalidFeedback val_repass"></span>

            <button id="submit" type="submit" name="Register" value="Sin up">Submit</button>

            <p class="options">Have account? <a href="<?php echo URLROOT; ?>/users/login">Sign in!</a></p>
        </form>
    </div>
</div>