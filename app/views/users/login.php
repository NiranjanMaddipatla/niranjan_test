<div class="container-login">
    <div class="wrapper-login">
        <h2>Sign in</h2>
            <span class="invalidFeedback val_status"></span>
        <form id="login" action="" method ="POST">
            <input type="text" placeholder="User Emial *" name="email">
            <span class="invalidFeedback val_email"> </span>

            <input type="password" placeholder="Password *" name="pass">
            <span class="invalidFeedback val_pass"></span>

            <button id="submit" type="submit" name="Login" value="Log in">Submit</button>

            <p class="options">Not registered yet? <a href="<?php echo URLROOT; ?>/users/join">Create an account!</a></p>
        </form>
    </div>
</div>