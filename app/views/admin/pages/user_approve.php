<div class="container-login">
    <div class="wrapper-login">
        <h2>Re-Approve Requests</h2>
           <span class="invalidFeedback val_status"></span>
            <table id="users_requests">
                <tbody>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                  </tr>
                <?php foreach($data['content'] as $value):?>
                    <tr>
                          <td>
                                <?php echo $value['name']; ?>
                          </td>
                          <td>
                                <?php echo $value['email']; ?>
                          </td>
                          <td>
                               <button class="approve" id="<?php echo $value['ID']; ?>" type="submit">ReApprove</button>
                               <button class= "dis_approve" id="<?php echo $value['ID']; ?>" type="submit">Delete</button>
                          </td>

                    </tr>
                    <?php endforeach; if(empty($data['content'])) :?>
                            <tr><td colspan="4" style= "text-align: center;">--- No Data Available ---</td><tr>
                    <?php  endif; ?>
                </tbody>
            </table>
    </div>
</div>