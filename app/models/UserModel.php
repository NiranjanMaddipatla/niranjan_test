<?php

use app\libraries as library;
    class UserModel{
        private $db ;
        public function __construct(){
            $this->db = new library\Database;
        }
        public function getUsers(){
            $this->db->query("select * from user_data where is_live!='-1' and type <>'1'");
            return $this->db->resultSet();
        }
        public function getPendingUsers(){
            $this->db->query("select * from user_data where is_live=0 and type <>'1'");
            return $this->db->resultSet();
        }

        public function getUser($email){
            $this->db->query("SELECT * FROM user_data where email=:email");
            $this->db->bind(":email",$email,null);
            return $this->db->single();
        }

        public function approvePendingUser($id){
            $this->db->query("UPDATE user_data set is_live='1' where ID=:id");
            $this->db->bind(":id",$id,null);
            return $this->db->execute();
        }
        public function reApprovePendingUser($id){
            $this->db->query("UPDATE user_data set is_live='1' where ID=:id");
            $this->db->bind(":id",$id,null);
            return $this->db->execute();
        }
        public function getReApprovePendingUsers(){
            $this->db->query("select * from user_data where is_live ='-1' and type <>'1'");
            return $this->db->resultSet();
        }
        public function  deletePendingUser($id){
            $this->db->query("Delete from user_data where ID=:id");
            $this->db->bind(":id",$id,null);
            return $this->db->execute();
        }

         public function rejectPendingUser($id){
            $this->db->query("UPDATE user_data set is_live='-1' where ID=:id");
            $this->db->bind(":id",$id,null);
            return $this->db->execute();
        }

        public function insertUser($data){
            $this->db->query("INSERT INTO user_data (email,name,password) values(:email,:name,:password)");
            $this->db->bind(":email",$data['email'],null);
            $this->db->bind(":name",$data['name'],null);
            $this->db->bind(":password",$data['pass'],null);
            $this->db->execute();
            return $this->db->rowCount();

        }
    }
?>