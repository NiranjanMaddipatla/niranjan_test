<?php

use app\libraries as library;
    class PostModel{
        private $db ;
        public function __construct(){
            $this->db = new library\Database;
        }
        public function getPosts(){
            $this->db->query("select * from post_data");
            return $this->db->resultSet();
        }
        public function getPost($post_id){
            $this->db->query("SELECT * FROM post_data where post_id=:post_id");
            $this->db->bind(":post_id",$post_id,null);
            return $this->db->single();
        }

        public function insertPost($data,$id,$name){
            $this->db->query("INSERT INTO post_data (user_id,post_title,post_desc,user_name) values(:user_id,:post_title,:post_desc,:user_name)");
            $this->db->bind(":user_id",$id,null);
            $this->db->bind(":post_title",$data['post_name'],null);
            $this->db->bind(":post_desc",$data['post_desc'],null);
            $this->db->bind(":user_name",$name,null);
            $this->db->execute();
            return $this->db->rowCount();
        }
        public function  deletePost($id){
            $this->db->query("DELETE FROM post_data where post_id = :post_id");
            $this->db->bind(":post_id",$id,null);
            $this->db->execute();
            return $this->db->rowCount();
        }
    }
?>