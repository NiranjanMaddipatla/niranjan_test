<?php
namespace app\helpers;
    class Session{
        public function __construct(){
            session_start();
        }

    public function create_session($key, $value){
        $_SESSION[$key] = $value;
    }
    public function unset_session($key){
        unset($_SESSION[$key]);
    }

    public function destroy_session(){
        if(!empty($_SESSION))
            session_destroy();
    }
    public function redirect($page){
        header('location:'.URLROOT.'/'.$page);
    }

   }
?>