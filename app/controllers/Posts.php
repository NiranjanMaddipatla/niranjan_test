<?php
use app\helpers as helper;
use app\libraries as library;
    class Posts  extends library\Controller{
        private $session;
        public function __construct(){
            $this->postModels = $this->model('PostModel');
            $this->session = new helper\Session;
        }

        public function index(){
            $postData = $this->postModels->getPosts();
            $data = [
                "title" => "Tradus Home Page",
                "content" => $postData
            ];
             $this->view("users/include/header",$data);
             $this->view("users/pages/index",$data);
             $this->view("users/include/footer");
        }

        public function about(){
            $data = [
                "title" => "About US Page",
                "content" => "Great to meet you! We are TRADUS – your smarter global heavy machinery marketplace. We connect professional sellers with serious buyers to trade used commercial vehicles and pre-owned heavy machinery. Find out how we save our members time and money while boosting their exposure across and beyond Europe."
            ];
             $this->view("users/include/header",$data);
             $this->view("users/pages/about",$data);
             $this->view("users/include/footer");
        }
        public function drag_drop(){
            $data = [
                    "title" => 'Drag and Drop your file'
                ];
                $this->view("users/pages/drag_drop");
        }

        public function id($id = ""){
            if(!empty($id)){
            
                $postData = $this->postModels->getPost($id);
                $data = [
                    "title" => $postData['post_title'],
                    "content" => $postData
                ];

                $this->view("users/include/header",$data);
                $this->view("users/pages/id",$data);
                $this->view("users/include/footer");
            }else{
                $this->session->redirect('posts');
            }
        }

         public function add_post(){
            if($_SESSION['user_live']!=1){
                $this->session->redirect('users/login');
             }
            if($_SERVER['REQUEST_METHOD']=='POST'){
                  $data = [
                    "post_name" => $_POST['post_name'],
                    "post_desc" => $_POST['post_desc'],
                    "post_name_error" => "",
                    "post_desc_error" => "",
                    "result" => "success"
                  ];
                $validate =true;
                if(empty($data['post_name'])){
                     $data["post_name_error"]="Title is a required field.";
                     $validate =false;
                }else if(str_word_count($data['post_name'])<5){
                     $data["email_error"]="Title is a not valid.";
                     $validate =false;
                }
                if(empty($data['post_desc'])){
                    $validate =false;
                    $data["post_desc_error"]="Description is required field.";
                }else{
                    if(str_word_count($data['post_desc'])<5){
                         $data["post_desc_error"]="Description is invalid.";
                         $validate =false;
                    }
                }
                if(!$validate){
                    $data["result"]="error";
                    echo json_encode($data);
                    exit;
                }

                 if(!empty($_SESSION['user_name'])){
                        $postInfo = $this->postModels->insertPost($data,$_SESSION['user_id'],$_SESSION['user_name']);
                        $data["result"]="success";
                        $data["post_desc"]="";
                        $data["post_name"]="";
                 }else{
                         $data["result"]="error";
                         $data["post_name_error"]="Your are not an authorized person to post please log in";
                 }
                 echo json_encode($data);
            }else if(!empty($_SESSION['user_name'])){
                  $data = [
						'title' => 'Tradus Posting Form'
                  ];
                    $this->view("users/include/header",$data);
                    $this->view("users/pages/add_post",$data);
                    $this->view("users/include/footer");

            }else{
                $this->session->redirect('users/login');
            }
        }


    }

?>