<?php
use app\libraries as library;
use app\helpers as helper;
    class Admin extends library\Controller{
        private $session;
        private $postModels;
        private $userModel;
        public function __construct(){
             $this->session = new helper\Session;
             $this->postModels = $this->model('PostModel');
             $this->userModel = $this->model('UserModel');
             if(!isset($_SESSION['user_name']) || $_SESSION['user_type']==0){
                $this->session->redirect('users/login');
             }
        }

        public function index(){
            $data = [
               "title" => "Error Page",
               "content" => "You are in a wrong place"
              ];
             $this->view("users/include/header",$data);
             $this->view("admin/error",$data);
             $this->view("users/include/footer");
        }
         public function approve(){
            if($_SERVER['REQUEST_METHOD']=='POST'){
                  $data = [
                   "title" => "Approve Page",
                   "content" => "",
                   "result" => "success"
                  ];
                 if($_POST['type']=='approve'){
                    $data['result'] = "success";
                    $request_data = $this->userModel->approvePendingUser($_POST['User_Id']);
                }else if($_POST['type']=='dis_approve'){
                    $data['result'] = "success_delete";
                    $request_data = $this->userModel->rejectPendingUser($_POST['User_Id']);
                }else{
                    $data['result'] = "error";
                }
                 $request_data = $this->userModel->getPendingUsers();
                 $data["content"] = json_encode($request_data);
                 echo json_encode($data);
            }else{
                $request_data = $this->userModel->getPendingUsers();
                $data = [
                   "title" => "Approve Page",
                   "content" => $request_data
                  ];
                 $this->view("users/include/header",$data);
                 $this->view("admin/pages/approve",$data);
                 $this->view("users/include/footer");
            }
         }
         public function all_users(){
                 if($_SERVER['REQUEST_METHOD']=='POST'){
                    $data = [
                     "title" => "Approve Page",
                     "content" => "",
                     "result" => "success"
                    ];
                    if($_POST['type']=='approve'){
                       $request_data = $this->userModel->approvePendingUser($_POST['User_Id']);
                    }else if($_POST['type']=='dis_approve'){
                       $request_data = $this->userModel->rejectPendingUser($_POST['User_Id']);
                        $data['result'] = "dis_approve";
                    }else{
                       $data['result'] = "error";
                    }
                    $request_data = $this->userModel->getUsers();
                    $data["content"] = json_encode($request_data);
                    echo json_encode($data);
                 }else{
                    $postInfo = $this->userModel->getUsers();
                    $data = [
                        "title" => "Actions Page",
                        "content" => $postInfo
                    ];
                    $this->view("users/include/header",$data);
                    $this->view("admin/pages/all_users",$data);
                    $this->view("users/include/footer");
                 }

         }


         public function post_actions(){
             if($_SERVER['REQUEST_METHOD']=='POST'){
                     if($this->postModels->deletePost($_POST['Post_Id'])){
                        $postData = json_encode($this->postModels->getPosts());
                        $data = [
                           "title" => "Post Actions Page",
                           "content" => $postData,
                           "result" => "success"
                         ];
                     }else{
                         $postData = json_encode($this->postModels->getPosts());
                        $data = [
                           "title" => "Post Actions Page",
                           "content" => $postData,
                           "result" => "error"
                         ];
                     }
                     echo json_encode($data);

             }else{
                 $postInfo = $this->postModels->getPosts();
                 $data = [
                   "title" => "Post Actions Page",
                   "content" => $postInfo
                 ];
                 $this->view("users/include/header",$data);
                 $this->view("admin/pages/actions",$data);
                 $this->view("users/include/footer");
             }
         }

         public function user_actions(){
            if($_SERVER['REQUEST_METHOD']=='POST'){
                  $data = [
                   "title" => "User Actions Page",
                   "content" => "",
                   "result" => "success"
                  ];
                 if($_POST['type']=='approve'){
                    $data['result'] = "success";
                    $request_data = $this->userModel->reApprovePendingUser($_POST['User_Id']);
                }else if($_POST['type']=='dis_approve'){
                    $data['result'] = "success";
                    $request_data = $this->userModel->deletePendingUser($_POST['User_Id']);
                }else{
                    $data['result'] = "error";
                }
                 $request_data = $this->userModel->getReApprovePendingUsers();
                 $data["content"] = json_encode($request_data);
                 echo json_encode($data);
            }else{
                $request_data = $this->userModel->getReApprovePendingUsers();
                $data = [
                   "title" => "User Approve Page",
                   "content" => $request_data
                  ];
                 $this->view("users/include/header",$data);
                 $this->view("admin/pages/user_approve",$data);
                 $this->view("users/include/footer");
            }
         }

    }

?>