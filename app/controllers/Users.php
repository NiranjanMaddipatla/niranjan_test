<?php
use app\libraries as library;
use app\helpers as helper;
    class Users  extends library\Controller{
        private $session;
        public function __construct(){
            $this->userModel = $this->model('UserModel');
            $this->session = new helper\Session;
        }

        public function index(){
            $data = [
               "title" => "Error Page",
               "content" => "You are in a wrong place"
              ];
             $this->view("users/include/header",$data);
             $this->view("users/error",$data);
             $this->view("users/include/footer");
        }
        public function login(){
            if($_SERVER['REQUEST_METHOD']=='POST'){
                $data = [
                     "email" => $_POST['email'],
                     "pass" => $_POST['pass'],
                     "email_error" => "",
                     "pass_error" => "",
                     "result" => "success"
                ];
                $validate =true;
                $email_regex 	= "/^[\w%_\-.\d]+@[\w.\-]+.[A-Za-z]{2,6}$/";
                if(empty($data['email'])){
                     $data["email_error"]="Email is a required field.";
                     $validate =false;
                }else if(!preg_match($email_regex,$data['email'])){
                     $data["email_error"]="Email is a not valid.";
                     $validate =false;
                }
                if(empty($data['pass'])){
                    $validate =false;
                    $data["pass_error"]="Password is required field.";
                }
                if(!$validate){
                    $data["result"]="error";
                    unset($data['email']);
                    unset($data['pass']);
                    echo json_encode($data);
                    exit;
                }
                 $userInfo = $this->userModel->getUser($data['email']);
                 if(!empty($userInfo[0])){
                        if(password_verify($data['pass'],$userInfo['password'])){
                             unset($data['email']);
                             unset($data['pass']);
                              $this->session->create_session("user_id",$userInfo['ID']);
                              $this->session->create_session("user_type",$userInfo['type']);
                              $this->session->create_session("user_name",$userInfo['name']);
                              $this->session->create_session("user_logged",true);
                              $this->session->create_session("user_live",$userInfo['is_live']);
                        }else{
                            $data["result"]="error";
                            $data["pass_error"]="Wrong password Please try with correct one";
                        }
                 }else{
                        $data["result"]="error";
                        $data["email_error"]="Email not registered with US. Please register and proceed";
                 }
                 unset($data['email']);
                 unset($data['pass']);
                 echo json_encode($data);
                 exit;
            }else{
            if(isset($_SESSION['user_name'])){
                    $this->session->redirect('posts/index');
               }else{
                   $data = [
						'title' => 'Tradus Login Page'
                    ];
                     $this->view("users/include/header",$data);
                     $this->view("users/login",$data);
                     $this->view("users/include/footer");
                }
            }
        }
        public function join(){
            if($_SERVER['REQUEST_METHOD']=='POST'){
                  $data = [
                    "email" => $_POST['email'],
                    "pass" => $_POST['pass'],
                    "name" => $_POST['name'],
                    "repass" => $_POST['repass'],
                    "email_error" => "",
                    "pass_error" => "",
                    "name_error" => "",
                    "repass_error" => "",
                    "result" => "success"
                  ];
                $validate =true;
                $email_regex 	= "/^[\w%_\-.\d]+@[\w.\-]+.[A-Za-z]{2,6}$/";
                if(empty($data['email'])){
                     $data["email_error"]="Email is a required field.";
                     $validate =false;
                }else if(!preg_match($email_regex,$data['email'])){
                     $data["email_error"]="Email is a not valid.";
                     $validate =false;
                }
                if(empty($data['pass'])){
                    $validate =false;
                    $data["pass_error"]="Password is required field.";
                }else{
                    $pass_regex = "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/";
                    if(!preg_match($pass_regex,$data['pass'])){
                         $data["pass_error"]="Password is invalid.";
                         $validate =false;
                    }
                }

                if(empty($data['name'])){
                    $validate =false;
                    $data["name_error"]="Name is required field.";
                }
                if(empty($data['repass'])){
                    $validate =false;
                    $data["repass_error"]="Confirm password is required field.";
                }else{
                    $pass_regex = "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/";
                    if(!preg_match($pass_regex,$data['repass'])){
                            $data["repass_error"]="Confirm password is invalid.";
                            $validate =false;
                    }
                }
                if($data['pass']!=$data['repass']){
                    $data["repass_error"]="Confirm password is not matching.";
                    $data["pass_error"]="Password not matching.";
                    $validate =false;
                }
                if(!$validate){
                    $data["result"]="error";
                    unset($data['email']);
                    unset($data['pass']);
                    unset($data['repass']);
                    echo json_encode($data);
                    exit;
                }
                $userInfo = $this->userModel->getUser($data['email']);

                 if(empty($userInfo[0])){
                        $data['pass'] = password_hash($data['pass'],PASSWORD_DEFAULT);
                        $userInfo = $this->userModel->insertUser($data);
                        $data["result"]="success";
                        unset($data['email']);
                        unset($data['pass']);
                        unset($data['repass']);

                 }else{
                         $data["result"]="error";
                         $data["email_error"]="Email already registered with US. Please login.";
                 }
                    unset($data['email']);
                    unset($data['pass']);
                    unset($data['repass']);
                 echo json_encode($data);
                 exit;
            }else {
             if(isset($_SESSION['user_name'])){
                    $this->session->redirect('posts/index');
               }else{
                  $data = [
					'title' => 'Tradus Signup Page'
                  ];
                $this->view("users/include/header",$data);
                $this->view("users/join",$data);
                $this->view("users/include/footer");
              }
            }
        }

        public function logout(){
            $this->session->destroy_session();
            $this->session->redirect('users/login');
        }


    }

?>